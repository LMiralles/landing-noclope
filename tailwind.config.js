module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'dark-blue': '#112E4D',
        'green-brand': '#31D0B5'
      },
      fontFamily: {
        sans: ["Lato"]
      },
      boxShadow: {
        'float': '3px 4px 25px rgba(0, 0, 0, 0.1)'
      },
      borderRadius: {
        '4xl': '3rem'
      },
      rotate: {
        '23': '23deg',
        '-23': '-23deg'
      },
      outline: {
        'green': '2px solid #31D0B5'
      },
      width: {
        'fit': 'fit-content'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
