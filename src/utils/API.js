import axios from "axios";

const headers = {
    "Content-Type": "application/json"
};

const burl = "https://noclope.eu/api/admin";

export default {
    collectEmail: (send) => {
        return axios.post(`${burl}/collectEmail`, send, { headers: headers })
    }
}