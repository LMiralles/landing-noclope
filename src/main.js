import { createApp } from 'vue'
import App from './App.vue'
require("typeface-lato")
import './assets/styles/tailwind.css'

import BaseInput from './components/base/BaseInput'
import BaseIcon from './components/base/BaseIcon'


const app = createApp(App)

app.component('BaseInput', BaseInput)
app.component('BaseIcon', BaseIcon)

app.mount('#app')
